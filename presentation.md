<style type="text/css">
  .reveal p {
    text-align: left;
  }
  .reveal ul {
    display: block;
  }
  .reveal ol {
    display: block;
  }
</style>

# SRE

---

## Who are we?
<div style="display: inline-block">
  <img  src="http://whoswho.mintel.com/profile/nbadger@mintel.com/image/?size=200x200">
  <div>Nick <br> Badger</div>
</div>
<div style="display: inline-block">
  <img  src="http://whoswho.mintel.com/profile/fciocchetti@mintel.com/image/?size=200x200">
  <div>Francesco <br> Ciocchetti</div>
</div>
<div style="display: inline-block">
  <img  src="http://whoswho.mintel.com/profile/cward@mintel.com/image/?size=200x200">
  <div>Charlie<br>Ward</div>
</div>

---

## Who are we? (pt2)
<div style="display: inline-block">
  <img  src="http://whoswho.mintel.com/profile/aharrison-fuller@mintel.com/image/?size=200x200">
  <div>Adam<br>H-F</div>
</div>
<div style="display: inline-block">
  <img  src="http://whoswho.mintel.com/profile/obristow@mintel.com/image/?size=200x200">
  <div>Oliver<br>Bristow</div>
</div>
<div style="display: inline-block">
  <img  src="http://whoswho.mintel.com/profile/bbrockway@mintel.com/image/?size=200x200">
  <div>Bobby<br>Brockway</div>
</div>

---

## What have we done so far?
* On prem cluster <!-- .element: class="fragment" -->
* Hand rolled K8's on GCP <!-- .element: class="fragment" -->
* Image Service on K8's <!-- .element: class="fragment" -->

---

## Satoshi 2.0
* GKE <!-- .element: class="fragment" -->
* Hashicorp Vault <!-- .element: class="fragment" -->
* GitOps <!-- .element: class="fragment" -->

---

## Decisions / What are we using and why

---

## Tools (not comprehensive)
* Terraform / Terragrunt
* GPG / Git-Crypt
* Gitlab-CI
* jsonnet
* Kustomise
* Ansible (Satoshi 1.0)

---

## Current Cluster Capabilities.
* Prometheus & Alertmanager
* Grafana
* EFK
* Redis
* HAProxy Ingress

---

### Cluster implementation details
* Kubernetes Network Policies.
* CNI - Calico
* Resource Quotas per namespace
* Limits and Requests
* Pod security policies.
* RBAC
* Service Discovery via K8's
* Secrets management though Bitnami's SealedSecrets

---

## IAM

- CoreOS Dex as an IDP gateway to talk OIDC (against shibboleth.mintel.ad / LDAP
- GKE using gcloud auth (isolated from our other mintel google groups)
- No staging / production access for developers
- Users locked down to specific namespaces(team areas) on dev cluster

---

## Google Cloud Services
* DNS
* GKE
* Compute
* CloudSQL
* Buckets
* Google Storage (persistant disks).

---

## Alerting
* Alertmanager / Prometheus
* OpsGenie.
* Triage
* Eventual **official** on-call

Annotation based alerts, routed to teams.