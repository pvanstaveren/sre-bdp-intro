all: init build

init:
	git submodule update --init --recursive
	(cd reveal.js && npm install)
	rm reveal.js/index.html
	(cd reveal.js && ln -s ../index.html index.html && ln -s ../presentation.md presentation.md)

build:
	(cd reveal.js && npm start)

.PHONY: init build